import React,{useState} from 'react'
import TodoInputComponent from './TodoInputComponent'
import TodoOutputComponent from './TodoOutputComponent'
  const TodoComponent=()=>{
    const [usersDetails, setUserDetails] = useState([]);

    const setUsers = (userData) => {
        console.log('session',userData);
        // pushing the form data
        setUserDetails([...usersDetails,userData])
    }
    return (
        <div>
       <TodoInputComponent setUserDetails={setUsers}/>
       {
         usersDetails.length>0?<TodoOutputComponent users={usersDetails}/>:null
       }
       
        </div>
    );
}
export default TodoComponent;