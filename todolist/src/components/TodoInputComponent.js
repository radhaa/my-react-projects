import React ,{useState} from 'react';
   const TodoInputComponent=(props)=>{
    const [name, setName] = useState('');
    const [createdAt, setCreated] = useState(new Date());
    const [description, setDesc] = useState('');

    const handleNameChange = (e) => {
      console.log(e.target.value);
      
        setName(e.target.value);
    }

    const handleDateChange = (e) => {
        setCreated(e);
    }

    const handleDescChange = (e) => {
      console.log(e.target.value);

        setDesc(e.target.value);
    }

    const handleSubmit = () => {
        if(name.length === 0 || description.length === 0){
            alert('required fields should be filled');
        }
        else{
            setName('');
            setDesc('');
            setCreated(new Date());
            props.setUserDetails({name,description,createdAt});
        }
    }
  
    return (
        <div>
        <div class='leftdiv'>
          
            <input type='text' placeholder='name' id='name' value={name} onChange={handleNameChange}/>
            <br/>
            <input type='text' placeholder='date' id='date' onChange={handleDateChange} value={createdAt} />
            <br/>
            <input type='text' placeholder='description'id='description'  value={description} onChange={handleDescChange}/>
            <br/>
            <button type='submit' onClick={handleSubmit}>submit</button>
          
        </div>
        
        </div>
    );
}
export default TodoInputComponent;
